﻿using ConsoleEShop.Menu;

namespace ConsoleEShop
{
    static class Program
    {
        static void Main(string[] args)
        {
            Menu.Menu menu = new Menu.Menu();
            menu.Start();
        }
    }
}
