﻿using ConsoleEShop.BLL.DI;
using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.BLL.Services;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Menu
{
    internal partial class Menu
    {
        readonly IItemService _itemService;
        readonly IUserService _userService;
        readonly IOrderService _orderService;
        readonly IOrderStatusService _orderStatusService;
        readonly IRoleService _roleService;
        readonly IBasketService _basketService;
        readonly IItemCategoryService _itemCategoryService;

        bool isShouldClose;
        bool isAdmin;
        bool isUser;
        int currentUserId;

        public delegate void Command();
        public const string success = "Success!";
        public const string tryAgain = "Try again!";

        public Menu()
        {
            IKernel kernel = new StandardKernel(new ServiceNinjectModule());
            isShouldClose = false;
            isAdmin = false;
            isUser = false;

            _itemService = kernel.Get<ItemService>();
            _userService = kernel.Get<UserService>();
            _orderService = kernel.Get<OrderService>();
            _orderStatusService = kernel.Get<OrderStatusService>();
            _roleService = kernel.Get<RoleService>();
            _basketService = kernel.Get<BasketService>();
            _itemCategoryService = kernel.Get<ItemCategoryService>();
        }

        private void GetGuestMenu()
        {
            Dictionary<string, Command> commands = new Dictionary<string, Command>()
            {
                {"getItems", GetItems},
                {"findItem", FindByName},
                {"signUp", SignUp },
                {"signIn", SignIn },
                {"signOut", SignOut },
                {"clearConsole", Console.Clear },
                {"exit", Exit }
            };

            GetAvailableCommands(commands);

            ExecuteCommandsWhileShouldntExit(commands);
        }

        private void SignUp()
        {
            Console.WriteLine("Input login: ");
            string login = Console.ReadLine();
            Console.WriteLine("Input password: ");
            string password = Console.ReadLine();
            Console.WriteLine("Input your name: ");
            string name = Console.ReadLine();

            if (_userService.Get(x => x.Login == login).FirstOrDefault() == null)
            {
                _userService.Create(login, password, name);
                Console.WriteLine(success);
            }
            else
            {
                Console.WriteLine("User already exists!");
            }
        }

        public void Start()
        {
            GetMenu();
        }

        private void SignIn()
        {
            Console.WriteLine("Input login: ");
            string login = Console.ReadLine();
            Console.WriteLine("Input password: ");
            string password = Console.ReadLine();

            var user = _userService.SignIn(login, password);

            if (user == null)
            {
                Console.WriteLine(tryAgain);
            }
            else if (_roleService.FindById(user.Role_Id).Name == "admin")
            {
                isAdmin = true;
                Console.WriteLine(success);
            }
            else if (_roleService.FindById(user.Role_Id).Name == "user")
            {
                isUser = true;
                Console.WriteLine(success);
            }

            Start();
        }

        private void SignOut()
        {
            Console.Clear();
            currentUserId = 0;
            isAdmin = false;
            isUser = false;
            Console.WriteLine(success);
            Start();
        }

        private void FindByName()
        {
            Console.WriteLine("Input item name: ");
            string name = Console.ReadLine();
            var item = _itemService.Get(x => x.Name == name).FirstOrDefault();
            if(item != null)
            {
                Console.WriteLine(GetItemFullInfo(item.Id));
            }
            else
            {
                Console.WriteLine(tryAgain);
            }
        }

        private void Exit()
        {
            isShouldClose = true;
        }

        private void GetItems()
        {
            var items = _itemService.GetAll();

            foreach (var item in items)
            {
                Console.WriteLine(GetItemFullInfo(item.Id));
            }
        }

        private void GetMenu()
        {
            if (isAdmin)
            {
                GetAdminMenu();
            }
            else if (isUser)
            {
                GetUserMenu();
            }
            else
            {
                GetGuestMenu();
            }
        }

        private void ExecuteCommandsWhileShouldntExit(Dictionary<string, Command> commands)
        {
            while (!isShouldClose)
            {
                string command = Console.ReadLine();
                bool commandExist = false;

                foreach (var item in commands)
                {
                    if (command == item.Key)
                    {
                        item.Value.Invoke();
                        commandExist = true;
                        break;
                    }
                }

                if (!commandExist)
                {
                    Console.WriteLine("Command not found! Type 'help' to see all commands");
                }
            }
        }

        private string GetItemFullInfo(int id)
        {
            var item = _itemService.FindById(id);

            if (item != null)
            {
                string category = _itemCategoryService.FindById(item.Category_Id).Name;
                return $"- {item.Id} {item.Name} {category} {item.Price}\n{item.Description}";
            }

            return string.Empty;
        }

        private void GetAvailableCommands(Dictionary<string, Command> commands)
        {
            Console.WriteLine("Command list: ");
            if(commands != null)
            {
                foreach (var item in commands)
                {
                    Console.WriteLine(item.Key);
                }
            }
        }
    }
}
