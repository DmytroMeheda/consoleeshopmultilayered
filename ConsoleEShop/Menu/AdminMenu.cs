﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Menu
{
    internal partial class Menu
    {
        private void GetAdminMenu()
        {
            Dictionary<string, Command> commands = new Dictionary<string, Command>()
            {
                {"getItems", GetItems},
                {"findItem", FindByName},
                {"signOut", SignOut },
                {"addToBasket", AddItemToBasket },
                {"getOrderItems", GetBasketItems },
                {"registerOrder", RegisterOrder },
                {"clearConsole", Console.Clear },
                {"changeStatus", ChangeStatusByAdmin },
                {"getUserInfo", GetUserInfoByAdmin },
                {"editUser", EditUser },
                {"addItem", AddItem },
                {"editItem", EditItem },
                {"exit", Exit }
            };

            GetAvailableCommands(commands);

            ExecuteCommandsWhileShouldntExit(commands);
        }

        private void AddItem()
        {
            Console.WriteLine("Input new item name: ");
            string name = Console.ReadLine();

            GetAvailableCategories();
            Console.WriteLine("Input category: ");
            string categoryName = Console.ReadLine();
            var category = _itemCategoryService.Get(x => x.Name == categoryName).FirstOrDefault();

            if (category == null)
                return;

            Console.WriteLine("Input price: ");
            string priceStr = Console.ReadLine();

            if (!double.TryParse(priceStr, out double price))
                return;

            Console.WriteLine("Input description: ");
            string desc = Console.ReadLine();

            _itemService.Create(name, category.Id, price, desc);
        }

        private void GetUserInfoByAdmin()
        {
            Console.WriteLine("Input user id: ");
            string inputId = Console.ReadLine();
            if (!int.TryParse(inputId, out int id))
                return;
            Console.WriteLine(UserInfo(id));
        }

        private void EditUser()
        {
            Console.WriteLine("Input user id: ");
            string idStr = Console.ReadLine();

            if (!int.TryParse(idStr, out int id))
                return;

            var user = _userService.FindById(id);

            if (user == null)
                return;

            Console.WriteLine("Input new login:");
            string login = Console.ReadLine();

            if (_userService.Get(x => x.Login == login).FirstOrDefault() != null)
                return;

            Console.WriteLine("Input new password: ");
            string password = Console.ReadLine();
            Console.WriteLine("Input new name: ");
            string name = Console.ReadLine();

            user.Login = login;
            user.Password = password;
            user.Name = name;
            Console.WriteLine(success);
        }

        private void EditItem()
        {
            Console.WriteLine("Input user id: ");
            string idStr = Console.ReadLine();

            if (!int.TryParse(idStr, out int id))
                return;

            var item = _itemService.FindById(id);
            if (item == null)
                return;

            Console.WriteLine("Input new item name: ");
            string name = Console.ReadLine();

            GetAvailableCategories();
            Console.WriteLine("Input category: ");
            string categoryName = Console.ReadLine();
            var category = _itemCategoryService.Get(x => x.Name == categoryName).FirstOrDefault();

            if (category == null)
                return;

            Console.WriteLine("Input price: ");
            string priceStr = Console.ReadLine();

            if (!double.TryParse(priceStr, out double price))
                return;

            Console.WriteLine("Input description: ");
            string desc = Console.ReadLine();

            item.Name = name;
            item.Price = price;
            item.Category_Id = category.Id;
            item.Description = desc;
            Console.WriteLine(success);
        }

        private void ChangeStatusByAdmin()
        {
            Console.WriteLine("Input order id: ");
            string inputId = Console.ReadLine();
            if (!int.TryParse(inputId, out int id) && !IsStatusChangeable(id))
                return;

            var order = _orderService.Get(x => x.Id == id).FirstOrDefault();
            GetStatuses();
            Console.WriteLine("Input status: ");
            string statusName = Console.ReadLine();
            var status = _orderStatusService.Get(x => x.Name == statusName).FirstOrDefault();

            if (order != null && status != null)
            {
                order.Status_Id = status.Id;
            }
        }

        private void GetAvailableCategories()
        {
            Console.WriteLine("\nAvailable categories: ");

            foreach (var item in _itemCategoryService.GetAll())
            {
                Console.WriteLine(item.Name);
            }
        }

        private void GetStatuses()
        {
            Console.WriteLine("All statuses: ");
            var statuses = _orderStatusService.GetAll();
            foreach (var item in statuses)
            {
                Console.WriteLine(item.Name);
            }
        }
    }
}
