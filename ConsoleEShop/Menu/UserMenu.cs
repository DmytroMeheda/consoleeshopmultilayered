﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop.Menu
{
    internal partial class Menu
    {
        private void GetUserMenu()
        {
            Dictionary<string, Command> commands = new Dictionary<string, Command>()
            {
                {"getItems", GetItems},
                {"findItem", FindByName},
                {"signOut", SignOut },
                {"addToBasket", AddItemToBasket },
                {"getOrderItems", GetBasketItems },
                {"registerOrder", RegisterOrder },
                {"cancelOrder", CancelOrder },
                {"clearConsole", Console.Clear },
                {"changeStatus", ChangeStatus },
                {"history", OrderHistory },
                {"getUserInfo", GetUserInfo },
                {"editPersonalInfo", EditPersonalInfo },
                {"exit", Exit }
            };

            GetAvailableCommands(commands);

            ExecuteCommandsWhileShouldntExit(commands);
        }

        private void AddItemToBasket()
        {
            Console.WriteLine("Input item name to add: ");
            string itemName = Console.ReadLine();
            var basket = _basketService.Get(x => x.User_Id == currentUserId).FirstOrDefault();
            if (basket == null)
            {
                _basketService.Create(currentUserId);
            }

            basket = _basketService.Get(x => x.User_Id == currentUserId).FirstOrDefault();
            var item = _itemService.Get(x => x.Name == itemName).FirstOrDefault();

            if (item != null)
            {
                _basketService.AddItemToBasket(basket.Id, item);
                Console.WriteLine(itemName + " was added");
            }
            else
            {
                Console.WriteLine(tryAgain);
            }
        }

        private void GetBasketItems()
        {
            var basket = _basketService.Get(x => x.User_Id == currentUserId).FirstOrDefault();

            if (basket != null)
            {
                foreach (var item in basket.Items)
                {
                    Console.WriteLine(GetItemFullInfo(item.Id));
                }
            }
        }

        private void RegisterOrder()
        {
            var basket = _basketService.Get(x => x.User_Id == currentUserId).FirstOrDefault();

            if(basket != null && basket.Items.Count > 0)
            {
                _orderService.Create(currentUserId, basket.Items);
                Console.WriteLine(success);
                basket.Items.Clear();
            }
        }

        private void CancelOrder()
        {
            Console.WriteLine("Input order id: ");
            string inputId = Console.ReadLine();
            var orders = _orderService.Get(x => x.User_Id == currentUserId);
            var order = orders.FirstOrDefault(x => x.Id == Convert.ToInt32(inputId));

            if (order != null)
            {
                order.Status_Id = _orderStatusService.Get(x => x.Name == "CancelledByUser").FirstOrDefault().Id;
            }
        }

        private void OrderHistory()
        {
            var orders = _orderService.Get(x => x.User_Id == currentUserId);

            if(orders.Any())
            {
                foreach (var item in orders)
                {
                    Console.WriteLine(GetOrderInfo(item.Id));
                }
            }
        }

        private void EditPersonalInfo()
        {
            var user = _userService.FindById(currentUserId);
            Console.WriteLine("Input new login: ");
            user.Login = Console.ReadLine();
            Console.WriteLine("Input new password: ");
            user.Password = Console.ReadLine();
            Console.WriteLine("Input new name: ");
            user.Name = Console.ReadLine();
        }

        private void ChangeStatus()
        {
            Console.WriteLine("Input order id: ");
            string inputId = Console.ReadLine();

            if (!int.TryParse(inputId, out int id) && !IsStatusChangeable(id))
                return;

            var orders = _orderService.Get(x => x.User_Id == currentUserId);
            var order = orders.FirstOrDefault(x => x.Id == Convert.ToInt32(inputId));

            if(order != null)
            {
                order.Status_Id = _orderStatusService.Get(x => x.Name == "Received").FirstOrDefault().Id;
            }
        }

        private void GetUserInfo()
        {
            Console.WriteLine(UserInfo(currentUserId));
        }

        private string GetOrderInfo(int id)
        {
            var order = _orderService.FindById(id);

            if(order != null)
            {
                string statusName = _orderStatusService.FindById(order.Status_Id).Name;
                StringBuilder sb = new StringBuilder($"Order {order.Id}, {statusName}, ToPay: {order.Price}\nItems: ");

                foreach (var item in order.Items)
                {
                    sb.Append("\n- " + item.Name);
                }

                return sb.ToString();
            }

            return string.Empty;
        }

        private string UserInfo(int id)
        {
            var user = _userService.FindById(id);
            if(user != null)
            {
                string role = _roleService.FindById(user.Role_Id).Name;
                return "Login: " + user.Login + "\nRole: " + role + "\nName: " + user.Name;
            }

            return string.Empty;
        }

        private bool IsStatusChangeable(int id)
        {
            List<string> notChangeableStatusNames = new List<string>() { "CancelledByUser", "CancelledByAdmin", "Received" };
            var order = _orderService.FindById(id);

            if (order == null)
                return false;

            var statusName = _orderStatusService.FindById(order.Status_Id).Name;

            return !notChangeableStatusNames.Contains(statusName);
        }
    }
}
