﻿using System;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> Get(Func<T, bool> predicate);
        T FindById(int id);
        void Create(T item);
        void Update(T item);
        void Remove(int id);
    }
}
