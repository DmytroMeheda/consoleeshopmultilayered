﻿using ConsoleEShop.DAL.Entities;

namespace ConsoleEShop.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Item> Items { get; }
        IRepository<Order> Orders { get; }
        IRepository<User> Users { get; }
        IRepository<ItemCategory> Categories { get; }
        IRepository<Role> Roles { get; }
        IRepository<OrderStatus> Statuses { get; }
        IRepository<Basket> Baskets { get; }

        void Save();
    }
}
