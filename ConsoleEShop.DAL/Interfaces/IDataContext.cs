﻿using ConsoleEShop.DAL.Entities;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.Interfaces
{
    public interface IDataContext
    {
        public List<Item> Items { get; set; }
        public List<Order> Orders { get; set; }
        public List<User> Users { get; set; }
        public List<ItemCategory> Categories { get; set; }
        public List<Basket> Baskets { get; set; }
        public List<Role> Roles { get; set; }
        public List<OrderStatus> OrderStatuses { get; set; }
    }
}
