﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using ConsoleEShop.DAL.Repositories;
using Moq;
using System;

namespace ConsoleEShop.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly IDataContext _db;

        private IRepository<Item> _items;
        private IRepository<Order> _orders;
        private IRepository<User> _users;
        private IRepository<ItemCategory> _categories;
        private IRepository<Role> _roles;
        private IRepository<OrderStatus> _statuses;
        private IRepository<Basket> _baskets;

        public UnitOfWork(IDataContext db)
        {
            _db = db;
        }

        public IRepository<Item> Items
        {
            get
            {
                if (_items == null)
                    _items = new ItemRepository(_db);
                return _items;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (_orders == null)
                    _orders = new OrderRepository(_db);
                return _orders;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (_users == null)
                    _users = new UserRepository(_db);
                return _users;
            }
        }

        public IRepository<ItemCategory> Categories
        {
            get
            {
                if (_categories == null)
                    _categories = new ItemCategoryRepository(_db);
                return _categories;
            }
        }

        public IRepository<Role> Roles
        {
            get
            {
                if (_roles == null)
                    _roles = new RoleRepository(_db);
                return _roles;
            }
        }

        public IRepository<OrderStatus> Statuses
        {
            get
            {
                if (_statuses == null)
                    _statuses = new OrderStatusRepository(_db);
                return _statuses;
            }
        }

        public IRepository<Basket> Baskets
        {
            get
            {
                if (_baskets == null)
                    _baskets = new BasketRepository(_db);
                return _baskets;
            }
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
