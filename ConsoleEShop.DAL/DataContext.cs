﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System.Collections.Generic;

namespace ConsoleEShop.DAL
{
    public class DataContext : IDataContext
    {
        public List<Item> Items { get; set; }
        public List<Order> Orders { get; set; }
        public List<User> Users { get; set; }
        public List<ItemCategory> Categories { get; set; }
        public List<Basket> Baskets { get; set; }
        public List<Role> Roles { get; set; }
        public List<OrderStatus> OrderStatuses { get; set; }

        public DataContext()
        {
            SeedData();
        }

        private void SeedData()
        {
            Item item1 = new Item() { Id = 1, Name = "PorschePanamera", Category_Id = 1, Price = 120000, Description = "GTS" };
            Item item2 = new Item() { Id = 2, Name = "Porsche911", Category_Id = 2, Price = 220000, Description = "Turbo" };
            Item item3 = new Item() { Id = 3, Name = "PorscheCayenne", Category_Id = 3, Price = 90000, Description = "S" };
            Items = new List<Item>() {item1, item2, item3 };

            Orders = new List<Order>();

            User admin = new User() { Id = 1, Login = "admin", Password = "admin", Role_Id = 1, Name = "admin" };
            User user = new User() { Id = 2, Login = "dima", Password = "dima", Role_Id = 2, Name = "Dima" };
            Users = new List<User>() { admin, user };

            ItemCategory category1 = new ItemCategory() { Id = 1, Name = "GranTourismo" };
            ItemCategory category2 = new ItemCategory() { Id = 2, Name = "Sport" };
            ItemCategory category3 = new ItemCategory() { Id = 3, Name = "SUV" };
            Categories = new List<ItemCategory>() { category1, category2, category3 };

            Baskets = new List<Basket>();

            Role adminRole = new Role() { Id = 1, Name = "admin" };
            Role userRole = new Role() { Id = 2, Name = "user" };
            Roles = new List<Role>() { adminRole, userRole };

            OrderStatus status1 = new OrderStatus() { Id = 1, Name = "New" };
            OrderStatus status2 = new OrderStatus() { Id = 2, Name = "CancelledByUser" };
            OrderStatus status3 = new OrderStatus() { Id = 3, Name = "CancelledByAdmin" };
            OrderStatus status4 = new OrderStatus() { Id = 4, Name = "Paid" };
            OrderStatus status5 = new OrderStatus() { Id = 5, Name = "Sent" };
            OrderStatus status6 = new OrderStatus() { Id = 6, Name = "Received" };
            OrderStatus status7 = new OrderStatus() { Id = 7, Name = "Done" };
            OrderStatuses = new List<OrderStatus>() { status1, status2, status3, status4, status5, status6, status7 };
        }
    }
}
