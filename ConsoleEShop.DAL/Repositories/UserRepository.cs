﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly IDataContext db;

        public UserRepository(IDataContext context) => db = context;

        public IEnumerable<User> GetAll() => db.Users;
        public IEnumerable<User> Get(Func<User, bool> predicate) => db.Users.Where(predicate);
        public User FindById(int id) => db.Users.Find(u => u.Id == id);

        public void Create(User item)
        {
            if (item != null)
                db.Users.Add(item);
        }

        public void Update(User item)
        {
            User userToUpd = db.Users.Find(u => u.Id == item.Id);

            if (userToUpd != null)
            {
                userToUpd.Name = item.Name;
            }
        } 

        public void Remove(int id)
        {
            User userToDel = db.Users.Find(u => u.Id == id);

            if (userToDel != null)
                db.Users.Remove(userToDel);
        }
    }
}
