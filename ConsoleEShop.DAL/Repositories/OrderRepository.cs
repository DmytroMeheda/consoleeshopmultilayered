﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        private readonly IDataContext db;

        public OrderRepository(IDataContext context) => db = context;

        public IEnumerable<Order> GetAll() => db.Orders;
        public IEnumerable<Order> Get(Func<Order, bool> predicate) => db.Orders.Where(predicate);
        public Order FindById(int id) => db.Orders.Find(o => o.Id == id);

        public void Create(Order item)
        {
            if (item != null)
                db.Orders.Add(item);
        }

        public void Update(Order item)
        {
            Order itemToUpd = db.Orders.Find(o => o.Id == item.Id);

            if (itemToUpd != null)
            {
                itemToUpd.Items = item.Items;
                itemToUpd.Status_Id = item.Status_Id;
                itemToUpd.User_Id = item.User_Id;
                itemToUpd.Price = item.Price;
            }
        }

        public void Remove(int id)
        {
            Order orderToDel = db.Orders.Find(o => o.Id == id);

            if (orderToDel != null)
                db.Orders.Remove(orderToDel);
        }
    }
}
