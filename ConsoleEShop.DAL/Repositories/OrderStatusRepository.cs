﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class OrderStatusRepository : IRepository<OrderStatus>
    {
        private readonly IDataContext db;

        public OrderStatusRepository(IDataContext context) => db = context;

        public IEnumerable<OrderStatus> GetAll() => db.OrderStatuses;
        public IEnumerable<OrderStatus> Get(Func<OrderStatus, bool> predicate) => db.OrderStatuses.Where(predicate);
        public OrderStatus FindById(int id) => db.OrderStatuses.Find(o => o.Id == id);

        public void Create(OrderStatus item)
        {
            if (item != null)
                db.OrderStatuses.Add(item);
        }

        public void Update(OrderStatus item)
        {
            OrderStatus statusToUpd = db.OrderStatuses.Find(o => o.Id == item.Id);

            if (statusToUpd != null)
            {
                statusToUpd.Name = item.Name;
            }
        } 

        public void Remove(int id)
        {
            OrderStatus orderToDel = db.OrderStatuses.Find(o => o.Id == id);

            if (orderToDel != null)
                db.OrderStatuses.Remove(orderToDel);
        }
    }
}
