﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class RoleRepository : IRepository<Role>
    {
        private readonly IDataContext db;

        public RoleRepository(IDataContext context) => db = context;

        public IEnumerable<Role> GetAll() => db.Roles;
        public IEnumerable<Role> Get(Func<Role, bool> predicate) => db.Roles.Where(predicate);
        public Role FindById(int id) => db.Roles.Find(r => r.Id == id);

        public void Create(Role item)
        {
            if (item != null)
                db.Roles.Add(item);
        }

        public void Update(Role item)
        {
            Role roleToUpd = db.Roles.Find(r => r.Id == item.Id);

            if (roleToUpd != null)
            {
                roleToUpd.Name = item.Name;
            }
        }

        public void Remove(int id)
        {
            Role roleToDel = db.Roles.Find(r => r.Id == id);

            if (roleToDel != null)
                db.Roles.Remove(roleToDel);
        } 
    }
}
