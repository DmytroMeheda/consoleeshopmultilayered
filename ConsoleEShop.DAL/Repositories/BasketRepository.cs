﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class BasketRepository : IRepository<Basket>
    {
        private readonly IDataContext db;

        public BasketRepository(IDataContext context) => db = context;

        public IEnumerable<Basket> GetAll() => db.Baskets;
        public IEnumerable<Basket> Get(Func<Basket, bool> predicate) => db.Baskets.Where(predicate);
        public Basket FindById(int id) => db.Baskets.Find(b => b.Id == id);

        public void Create(Basket item)
        {
            if(item != null)
                db.Baskets.Add(item);
        }

        public void Update(Basket item)
        {
            Basket basketToUpd = db.Baskets.Find(b => b.Id == item.Id);

            if (basketToUpd != null)
                basketToUpd.Items = item.Items;
        }

        public void Remove(int id)
        {
            Basket basketToDel = db.Baskets.Find(b => b.Id == id);

            if (basketToDel != null)
                db.Baskets.Remove(basketToDel);
        }
    }
}
