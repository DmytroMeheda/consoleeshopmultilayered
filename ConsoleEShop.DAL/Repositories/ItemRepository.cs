﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class ItemRepository : IRepository<Item>
    {
        private readonly IDataContext db;

        public ItemRepository(IDataContext context) => db = context;

        public IEnumerable<Item> GetAll() => db.Items;
        public IEnumerable<Item> Get(Func<Item, bool> predicate) => db.Items.Where(predicate);
        public Item FindById(int id) => db.Items.Find(i => i.Id == id);

        public void Create(Item item)
        {
            if (item != null)
                db.Items.Add(item);
        }

        public void Update(Item item)
        {
            Item itemToUpd = db.Items.Find(i => i.Id == item.Id);

            if (itemToUpd != null)
                itemToUpd.Name = item.Name;
        }

        public void Remove(int id)
        {
            Item itemToDel = db.Items.Find(i => i.Id == id);

            if (itemToDel != null)
                db.Items.Remove(itemToDel);
        }
    }
}
