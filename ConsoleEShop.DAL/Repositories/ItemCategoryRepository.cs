﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class ItemCategoryRepository : IRepository<ItemCategory>
    {
        private readonly IDataContext db;

        public ItemCategoryRepository(IDataContext context) => db = context;

        public IEnumerable<ItemCategory> GetAll() => db.Categories;
        public IEnumerable<ItemCategory> Get(Func<ItemCategory, bool> predicate)
            => db.Categories.Where(predicate);
        public ItemCategory FindById(int id) => db.Categories.Find(c => c.Id == id);

        public void Create(ItemCategory item)
        {
            if(item != null)
                db.Categories.Add(item);
        }

        public void Update(ItemCategory item)
        {
            ItemCategory categoryToUpd = db.Categories.Find(c => c.Id == item.Id);

            if(categoryToUpd != null)
                categoryToUpd.Name = item.Name;
        }
        
        public void Remove(int id)
        {
            ItemCategory categoryToDel = db.Categories.Find(c => c.Id == id);

            if (categoryToDel != null)
                db.Categories.Remove(categoryToDel);
        }
    }
}
