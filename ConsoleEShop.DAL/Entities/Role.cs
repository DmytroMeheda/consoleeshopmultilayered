﻿using ConsoleEShop.DAL.Interfaces;

namespace ConsoleEShop.DAL.Entities
{
    public class Role : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
