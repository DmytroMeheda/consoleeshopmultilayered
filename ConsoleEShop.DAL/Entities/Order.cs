﻿using ConsoleEShop.DAL.Interfaces;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.Entities
{
    public class Order : IEntity
    {
        public int Id { get; set; }
        public int User_Id { get; set; }
        public ICollection<Item> Items { get; set; }
        public int Status_Id { get; set; }
        public double Price { get; set; }
    }
}
