﻿using ConsoleEShop.DAL.Interfaces;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.Entities
{
    public class Basket : IEntity
    {
        public int Id { get; set; }
        public int User_Id { get; set; }
        public ICollection<Item> Items { get; set; }
    }
}
