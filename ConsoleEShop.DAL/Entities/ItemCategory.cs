﻿using ConsoleEShop.DAL.Interfaces;

namespace ConsoleEShop.DAL.Entities
{
    public class ItemCategory : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
