﻿using ConsoleEShop.DAL.Interfaces;

namespace ConsoleEShop.DAL.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int Role_Id { get; set; }
    }
}
