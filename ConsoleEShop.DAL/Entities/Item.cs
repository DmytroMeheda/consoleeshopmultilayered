﻿using ConsoleEShop.DAL.Interfaces;

namespace ConsoleEShop.DAL.Entities
{
    public class Item : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Category_Id { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
    }
}
