﻿using ConsoleEShop.DAL;
using ConsoleEShop.DAL.Interfaces;
using Ninject.Modules;

namespace ConsoleEShop.BLL.DI
{
    public class ServiceNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().InSingletonScope();
            Bind<IDataContext>().To<DataContext>();
        }
    }
}
