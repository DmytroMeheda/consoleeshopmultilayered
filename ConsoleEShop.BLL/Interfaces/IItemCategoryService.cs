﻿using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BLL.Interfaces
{
    public interface IItemCategoryService
    {
        ItemCategory FindById(int id);
        IEnumerable<ItemCategory> GetAll();
        IEnumerable<ItemCategory> Get(Func<ItemCategory, bool> predicate);
    }
}
