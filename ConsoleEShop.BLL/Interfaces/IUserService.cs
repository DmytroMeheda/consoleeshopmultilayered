﻿using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BLL.Interfaces
{
    public interface IUserService
    {
        IEnumerable<User> GetAll();
        IEnumerable<User> Get(Func<User, bool> predicate);
        User FindById(int id);
        void Create(string login, string password, string name);
        User SignIn(string login, string password);
        void Remove(int id);
    }
}
