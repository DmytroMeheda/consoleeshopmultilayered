﻿using ConsoleEShop.DAL.Entities;

namespace ConsoleEShop.BLL.Interfaces
{
    public interface IRoleService
    {
        Role FindById(int id);
    }
}
