﻿using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BLL.Interfaces
{
    public interface IItemService
    {
        IEnumerable<Item> GetAll();
        IEnumerable<Item> Get(Func<Item, bool> predicate);
        Item FindById(int id);
        void Create(string name, int category_id, double price, string description);
        void Remove(int id);
    }
}
