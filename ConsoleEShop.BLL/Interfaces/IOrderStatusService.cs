﻿using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BLL.Interfaces
{
    public interface IOrderStatusService
    {
        OrderStatus FindById(int id);
        IEnumerable<OrderStatus> GetAll();
        IEnumerable<OrderStatus> Get(Func<OrderStatus, bool> predicate);
    }
}
