﻿using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BLL.Interfaces
{
    public interface IBasketService
    {
        IEnumerable<Basket> GetAll();
        IEnumerable<Basket> Get(Func<Basket, bool> predicate);
        void AddItemToBasket(int id, Item item);
        Basket FindById(int id);
        void Create(int user_id);
        void Remove(int id);
    }
}
