﻿using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BLL.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<Order> GetAll();
        IEnumerable<Order> Get(Func<Order, bool> predicate);
        Order FindById(int id);
        void Create(int userId, ICollection<Item> items);
        void Remove(int id);
    }
}
