﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BLL.Services
{
    public class OrderStatusService : IOrderStatusService
    {
        private readonly IUnitOfWork _uow;
        public OrderStatusService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public OrderStatus FindById(int id) => _uow.Statuses.FindById(id);
        public IEnumerable<OrderStatus> GetAll() => _uow.Statuses.GetAll();

        public IEnumerable<OrderStatus> Get(Func<OrderStatus, bool> predicate) => _uow.Statuses.Get(predicate);
    }
}
