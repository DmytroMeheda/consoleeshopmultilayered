﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BLL.Services
{
    public class ItemCategoryService : IItemCategoryService
    {
        private readonly IUnitOfWork _uow;
        public ItemCategoryService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public ItemCategory FindById(int id) => _uow.Categories.FindById(id);

        public IEnumerable<ItemCategory> Get(Func<ItemCategory, bool> predicate) => _uow.Categories.Get(predicate);

        public IEnumerable<ItemCategory> GetAll() => _uow.Categories.GetAll();
    }
}
