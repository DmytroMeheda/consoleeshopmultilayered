﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _uow;
        public OrderService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<Order> GetAll() => _uow.Orders.GetAll();
        public IEnumerable<Order> Get(Func<Order, bool> predicate) => _uow.Orders.Get(predicate);
        public Order FindById(int id) => _uow.Orders.FindById(id);

        public void Create(int userId, ICollection<Item> items)
        {
            var order = new Order() { Items = items, User_Id = userId, Status_Id = 1 };
            order.Price = order.Items.Sum(x => x.Price);
            var orders = _uow.Orders.GetAll();

            if (!orders.Any())
            {
                order.Id = 1;
            }
            else
            {
                int maxId = orders.Max(i => i.Id);
                order.Id = maxId + 1;
            }

            _uow.Orders.Create(order);
        }

        public void Remove(int id) => _uow.Orders.Remove(id);
    }
}
