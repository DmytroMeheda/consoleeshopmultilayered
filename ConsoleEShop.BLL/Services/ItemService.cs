﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.BLL.Services
{
    public class ItemService : IItemService
    {
        private readonly IUnitOfWork _uow;
        public ItemService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<Item> GetAll() => _uow.Items.GetAll();
        public IEnumerable<Item> Get(Func<Item, bool> predicate) => _uow.Items.Get(predicate);
        public Item FindById(int id) => _uow.Items.FindById(id);

        public void Create(string name, int category_id, double price, string description)
        {
            var item = new Item() { Name = name, Category_Id = category_id, Price = price, Description = description };
            var items = _uow.Items.GetAll();

            if (!items.Any())
            {
                item.Id = 1;
            }
            else
            {
                int maxId = items.Max(i => i.Id);
                item.Id = maxId + 1;
            }

            _uow.Items.Create(item);
        }

        public void Remove(int id) => _uow.Items.Remove(id);
    }
}
