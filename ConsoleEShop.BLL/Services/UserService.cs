﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        public UserService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<User> GetAll() => _uow.Users.GetAll();
        public IEnumerable<User> Get(Func<User, bool> predicate) => _uow.Users.Get(predicate);
        public User FindById(int id) => _uow.Users.FindById(id);

        public void Create(string login, string password, string name)
        {
            var user = new User() { Login = login, Password = password, Name = name };
            var users = _uow.Users.GetAll();

            if (users.Any())
            {
                user.Id = 1;
            }
            else
            {
                int maxId = users.Max(u => u.Id);
                user.Id = maxId + 1;
            }

            _uow.Users.Create(user);
        }

        public void Remove(int id) => _uow.Users.Remove(id);

        public User SignIn(string login, string password)
        {
            return _uow.Users.Get(x => x.Login == login && x.Password == password).FirstOrDefault();
        }
    }
}
