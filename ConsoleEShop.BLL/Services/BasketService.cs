﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.BLL.Services
{
    public class BasketService : IBasketService
    {
        private readonly IUnitOfWork _uow;
        public BasketService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<Basket> GetAll() => _uow.Baskets.GetAll();
        public IEnumerable<Basket> Get(Func<Basket, bool> predicate) => _uow.Baskets.Get(predicate);
        public Basket FindById(int id) => _uow.Baskets.FindById(id);

        public void Create(int user_id)
        {
            var basket = new Basket() { User_Id = user_id };
            basket.Items = new List<Item>();
            var baskets = _uow.Baskets.GetAll();

            if (!baskets.Any())
            {
                basket.Id = 1;
            }
            else
            {
                int maxId = _uow.Baskets.GetAll().Max(i => i.Id);
                basket.Id = maxId + 1;
            }

            _uow.Baskets.Create(basket);
        }

        public void Remove(int id) => _uow.Baskets.Remove(id);

        public void AddItemToBasket(int id, Item item)
        {
            var basket = _uow.Baskets.FindById(id);

            if(basket != null)
            {
                basket.Items.Add(item);
            }
        }
    }
}
