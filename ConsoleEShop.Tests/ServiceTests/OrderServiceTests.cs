﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.BLL.Services;
using ConsoleEShop.DAL;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConsoleEShop.Tests.ServiceTests
{
    [TestFixture]
    class OrderServiceTests
    {
        IOrderService orderService;
        IUnitOfWork uow;
        Mock<IDataContext> mock;

        [SetUp]
        public void SetUp()
        {
            mock = new Mock<IDataContext>();
            uow = new UnitOfWork(mock.Object);
            orderService = new OrderService(uow);
        }

        [Test]
        public void GetAll_CollectionHasElements_ReturnNotEmptyCollection()
        {
            //arrange
            int emptyCollectionCount = 0;
            mock.Setup(x => x.Orders).Returns(new List<Order>()
            {
                new Order(){Id = 1},
                new Order(){Id = 2},
                new Order{Id = 3},
            });

            //act
            var actual = mock.Object.Orders.Count;

            //assert
            Assert.IsFalse(emptyCollectionCount == actual);
        }

        [Test]
        public void Get_CollectionHasElementByCondition_ReturnNotNull()
        {
            //arrange
            mock.Setup(x => x.Orders).Returns(new List<Order>()
            {
                new Order(){Id = 1},
                new Order(){Id = 2, Price = 999},
                new Order{Id = 3},
            });

            static bool predicate(Order x) => x.Price == 999;

            //act
            var actual = orderService.Get(predicate);

            //assert
            Assert.IsNotNull(actual);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [Test]
        public void FindById_CollectionHasElements_ReturnNotNull(int id)
        {
            //arrange
            mock.Setup(x => x.Orders).Returns(new List<Order>()
            {
                new Order(){Id = 1},
                new Order(){Id = 2},
                new Order{Id = 3},
            });

            //act
            var actual = orderService.FindById(id);

            //assert
            Assert.IsNotNull(actual);
        }

        [Test]
        public void Create_CollectionIsEmptyDataIsCorrect_CollectionCountIncreased()
        {
            //arrange
            int userId = 1;
            List<Item> items = new List<Item>() { new Item() };
            mock.Setup(x => x.Orders).Returns(new List<Order>()
            {
                new Order(){Id = 1},
                new Order(){Id = 2},
                new Order(){Id = 3},
            });
            int countBeforeCreate = mock.Object.Orders.Count;

            //act
            orderService.Create(userId, items);
            int actualCount = mock.Object.Orders.Count;

            //assert
            Assert.IsTrue(actualCount > countBeforeCreate);
        }

        [Test]
        public void Remove_CollectionHasItemWithId1_CollecttionCountDecreased()
        {
            //arrange
            mock.Setup(x => x.Orders).Returns(new List<Order>()
            {
                new Order(){Id = 1},
                new Order(){Id = 2},
                new Order(){Id = 3},
            });
            int idToRemove = 2;
            int countBeforeRemove = mock.Object.Orders.Count;

            //act
            orderService.Remove(idToRemove);
            int actualCount = mock.Object.Orders.Count;

            //assert
            Assert.IsTrue(actualCount < countBeforeRemove);
        }
    }
}
