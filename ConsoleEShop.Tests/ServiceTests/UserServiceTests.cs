﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.BLL.Services;
using ConsoleEShop.DAL;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConsoleEShop.Tests.ServiceTests
{
    [TestFixture]
    class UserServiceTests
    {
        IUserService userService;
        IUnitOfWork uow;
        Mock<IDataContext> mock;

        [SetUp]
        public void SetUp()
        {
            mock = new Mock<IDataContext>();
            mock.Setup(x => x.Users).Returns(new List<User>()
            {
                new User(){Id = 1, Login = "Login1", Password = "Password1"},
                new User(){Id = 2, Login = "Login2", Password = "Password2"},
                new User{Id = 3, Login = "Login3", Password = "Password3"}
            });
            uow = new UnitOfWork(mock.Object);
            userService = new UserService(uow);
        }

        [Test]
        public void GetAll_CollectionHasElements_ReturnNotEmptyCollection()
        {
            //arrange
            int emptyCollectionCount = 0;
            
            //act
            var actual = mock.Object.Users.Count;

            //assert
            Assert.IsFalse(emptyCollectionCount == actual);
        }

        [Test]
        public void Get_CollectionHasElementByCondition_ReturnNotNull()
        {
            //arrange
            string login = "Login228";
            mock.Object.Users.Add(new User() { Id = 4, Login = login });
            bool predicate(User x) => x.Login == login;

            //act
            var actual = userService.Get(predicate);

            //assert
            Assert.IsNotNull(actual);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [Test]
        public void FindById_CollectionHasElements_ReturnNotNull(int id)
        {
            //act
            var actual = userService.FindById(id);

            //assert
            Assert.IsNotNull(actual);
        }

        [Test]
        public void Create_CollectionIsEmptyDataIsCorrect_CollectionCountIncreased()
        {
            //arrange
            string login = "TestLogin";
            string password = "TestPass";
            string name = "TestName";
            int countBeforeCreate = mock.Object.Users.Count;

            //act
            userService.Create(login, password, name);
            int actualCount = mock.Object.Users.Count;

            //assert
            Assert.IsTrue(actualCount > countBeforeCreate);
        }

        [Test]
        public void Remove_CollectionHasItemWithId1_CollecttionCountDecreased()
        {
            //arrange
            int idToRemove = 2;
            int countBeforeRemove = mock.Object.Users.Count;

            //act
            userService.Remove(idToRemove);
            int actualCount = mock.Object.Users.Count;

            //assert
            Assert.IsTrue(actualCount < countBeforeRemove);
        }
    }
}
