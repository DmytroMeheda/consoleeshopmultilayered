﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.BLL.Services;
using ConsoleEShop.DAL;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Tests.ServiceTests
{
    [TestFixture]
    class ItemCategoryServiceTests
    {
        IItemCategoryService itemCategoryService;
        IUnitOfWork uow;
        Mock<IDataContext> mock;

        [SetUp]
        public void SetUp()
        {
            mock = new Mock<IDataContext>();
            mock.Setup(x => x.Categories).Returns(new List<ItemCategory>()
            {
                new ItemCategory(){Id = 1, Name = "Cat1"},
                new ItemCategory(){Id = 2, Name = "Cat2"},
                new ItemCategory(){Id = 3, Name = "Cat3"}
            });
            uow = new UnitOfWork(mock.Object);
            itemCategoryService = new ItemCategoryService(uow);
        }

        [Test]
        public void GetAll_CollectionHasElements_ReturnNotEmptyCollection()
        {
            //arrange
            int emptyCollectionCount = 0;

            //act
            var actual = itemCategoryService.GetAll();

            //assert
            Assert.AreNotEqual(emptyCollectionCount, actual.Count());
        }

        [Test]
        public void Get_CollectionHasElementByCondition_ReturnNotNull()
        {
            //arrange
            string name = "Cat123";
            ItemCategory cat = new ItemCategory() { Id = 86, Name = name };
            mock.Object.Categories.Add(cat);
            bool predicate(ItemCategory x) => x.Name == name;

            //act
            var actual = itemCategoryService.Get(predicate);

            //assert
            Assert.IsNotNull(actual);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [Test]
        public void FindById_CollectionHasElementWithId1_ReturnNotNull(int id)
        {
            //act
            var actual = itemCategoryService.FindById(id);

            //assert
            Assert.IsNotNull(actual);
        }
    }
}
