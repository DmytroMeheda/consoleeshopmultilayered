﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.BLL.Services;
using ConsoleEShop.DAL;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Tests.ServiceTests
{
    [TestFixture]
    class BasketServiceTests
    {
        IBasketService basketService;
        IUnitOfWork uow;
        Mock<IDataContext> mock;

        [SetUp]
        public void SetUp()
        {
            mock = new Mock<IDataContext>();
            List<Item> items = new List<Item>() { new Item() };
            mock.Setup(x => x.Baskets).Returns(new List<Basket>()
            {
                new Basket(){Id = 1, User_Id = 1, Items = items},
                new Basket(){Id = 2, User_Id = 1, Items = items},
                new Basket(){Id = 3, User_Id = 1, Items = items}
            });
            uow = new UnitOfWork(mock.Object);
            basketService = new BasketService(uow);
        }

        [Test]
        public void GetAll_CollectionHasElements_ReturnNotEmptyCollection()
        {
            //arrange
            int emptyCollectionCount = 0;

            //act
            var actual = mock.Object.Baskets.Count;

            //assert
            Assert.IsFalse(emptyCollectionCount == actual);
        }

        [Test]
        public void Create_CollectionNotEmptyDataIsCorrect_CollectionCountIncreased()
        {
            //arrange
            int userId = 1;
            int expectedCount = mock.Object.Baskets.Count + 1;

            //act
            basketService.Create(userId);
            int actual = basketService.GetAll().Count();

            //assert
            Assert.AreEqual(expectedCount, actual);
        }

        [Test]
        public void Get_CollectionHasElementByCondition_ReturnNotNull()
        {
            //arrange
            int user_id = 128;
            Basket basket = new Basket() { Id = 1, User_Id = user_id };
            mock.Object.Baskets.Add(basket);
            bool predicate(Basket x) => x.User_Id == user_id;

            //act
            var actual = basketService.Get(predicate);

            //assert
            Assert.IsNotNull(actual);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [Test]
        public void FindById_CollectionHasElementWithId1_ReturnNotNull(int id)
        {
            //act
            var actual = basketService.FindById(id);

            //assert
            Assert.IsNotNull(actual);
        }

        [Test]
        public void Remove_CollectionHasElementWithId1_CollectionCountDecreased()
        {
            //arrange
            int removeId = 999;
            Basket basket = new Basket() { Id = removeId};
            mock.Object.Baskets.Add(basket);
            int countBeforeRemove = mock.Object.Baskets.Count;

            //act
            basketService.Remove(removeId);
            int countAfterRemove = mock.Object.Baskets.Count;

            //assert
            Assert.IsTrue(countBeforeRemove > countAfterRemove);
        }

        [Test]
        public void AddItemToBasket_AddCorrectItemToEmptyBasket_CollectionCountIncreased()
        {
            //arrange
            var basket = mock.Object.Baskets[0];
            Item item = new Item() { Id = 1, Name = "Test" };
            int countBeforeAdd = basket.Items.Count;

            //act
            basketService.AddItemToBasket(basket.Id, item);
            int countAfterAdd = basket.Items.Count;

            //assert
            Assert.IsTrue(countBeforeAdd < countAfterAdd);
        }
    }
}
