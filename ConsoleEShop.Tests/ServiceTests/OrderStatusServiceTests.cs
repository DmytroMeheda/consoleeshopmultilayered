﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.BLL.Services;
using ConsoleEShop.DAL;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Tests.ServiceTests
{
    [TestFixture]
    class OrderStatusServiceTests
    {
        IOrderStatusService orderStatusService;
        IUnitOfWork uow;
        Mock<IDataContext> mock;

        [SetUp]
        public void SetUp()
        {
            var context = new DataContext();
            mock = new Mock<IDataContext>();
            mock.Setup(x => x.OrderStatuses).Returns(new List<OrderStatus>()
            {
                new OrderStatus(){Id = 1},
                new OrderStatus(){Id = 2},
                new OrderStatus{Id = 3}
            });
            uow = new UnitOfWork(mock.Object);
            orderStatusService = new OrderStatusService(uow);
        }

        [Test]
        public void GetAll_CollectionHasElements_ReturnNotEmptyCollection()
        {
            //arrange
            int emptyCollectionCount = 0;

            //act
            var actual = orderStatusService.GetAll();

            //assert
            Assert.AreNotEqual(emptyCollectionCount, actual.Count());
        }

        [Test]
        public void Get_CollectionHasElementByCondition_ReturnNotNull()
        {
            //arrange
            string name = "TestName";
            mock.Setup(x => x.OrderStatuses).Returns(new List<OrderStatus>()
            {
                new OrderStatus(){Id = 1},
                new OrderStatus(){Id = 2, Name = name},
                new OrderStatus{Id = 3}
            });
            Func<OrderStatus, bool> predicate = x => x.Name == name;

            //act
            var actual = orderStatusService.Get(predicate);

            //assert
            Assert.IsNotNull(actual);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [Test]
        public void FindById_CollectionHasElementWithId1_ReturnNotNull(int id)
        {
            //act
            var actual = orderStatusService.FindById(id);

            //assert
            Assert.IsNotNull(actual);
        }
    }
}
