﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.BLL.Services;
using ConsoleEShop.DAL;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Tests.ServiceTests
{
    [TestFixture]
    class ItemServiceTests
    {
        IItemService itemService;
        IUnitOfWork uow;
        Mock<IDataContext> mock;

        [SetUp]
        public void SetUp()
        {
            mock = new Mock<IDataContext>();
            mock.Setup(x => x.Items).Returns(new List<Item>()
            {
                new Item(){Id = 1},
                new Item(){Id = 2},
                new Item(){Id = 3}
            });
            uow = new UnitOfWork(mock.Object);
            itemService = new ItemService(uow);
        }

        [Test]
        public void GetAll_CollectionHasElements_ReturnNotEmptyCollection()
        {
            //arrange
            int emptyCollectionCount = 0;

            //act
            var actual = itemService.GetAll().Count();

            //assert
            Assert.AreNotEqual(emptyCollectionCount, actual);
        }

        [Test]
        public void Get_CollectionHasElementByCondition_ReturnNotNull()
        {
            //arrange
            string name = "TestName";
            Item item = new Item() { Id = 66, Name = name };
            mock.Object.Items.Add(item);
            bool predicate(Item x) => x.Name == name;

            //act
            var actual = itemService.Get(predicate);

            //assert
            Assert.IsNotNull(actual);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [Test]
        public void FindById_CollectionHasElements_ReturnNotNull(int id)
        {
            //act
            var actual = itemService.FindById(id);

            //assert
            Assert.IsNotNull(actual);
        }


        [Test]
        public void Create_CreateItemCollectionNotEmpty_CountIncreased()
        {
            //arrange
            int countBeforeCreate = mock.Object.Items.Count;
            string name = "test";
            int category_id = 1;
            double price = 111;
            string description = "desc";

            //act
            itemService.Create(name, category_id, price, description);
            int countAfterCreate = mock.Object.Items.Count;

            //assert
            Assert.IsTrue(countBeforeCreate < countAfterCreate);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [Test]
        public void Remove_CollectionHasElementWithId_CollectionHasntElementWithId(int id)
        {
            //arrange
            int countBeforeRemove = mock.Object.Items.Count;

            //act
            itemService.Remove(id);
            int countAfterRemove = mock.Object.Items.Count;

            //assert
            Assert.IsTrue(countBeforeRemove > countAfterRemove);
        }
    }
}
