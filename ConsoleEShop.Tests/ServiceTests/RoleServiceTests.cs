﻿using ConsoleEShop.BLL.Interfaces;
using ConsoleEShop.BLL.Services;
using ConsoleEShop.DAL;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConsoleEShop.Tests.ServiceTests
{
    [TestFixture]
    class RoleServiceTests
    {
        IRoleService roleService;
        IUnitOfWork uow;
        Mock<IDataContext> mock;

        [SetUp]
        public void SetUp()
        {
            mock = new Mock<IDataContext>();
            uow = new UnitOfWork(mock.Object);
            roleService = new RoleService(uow);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [Test]
        public void FindById_CollectionHasElements_ReturnNotNull(int id)
        {
            //arrange
            mock.Setup(x => x.Roles).Returns(new List<Role>()
            {
                new Role(){Id = 1},
                new Role(){Id = 2},
                new Role(){Id = 3}
            });

            //act
            var actual = roleService.FindById(id);

            //assert
            Assert.IsNotNull(actual);
        }
    }
}
